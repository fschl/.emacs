;;; go-eldoc-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (go-eldoc-setup) "go-eldoc" "go-eldoc.el" (21788
;;;;;;  4529 281818 143000))
;;; Generated autoloads from go-eldoc.el

(autoload 'go-eldoc-setup "go-eldoc" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("go-eldoc-pkg.el") (21788 4529 315985
;;;;;;  806000))

;;;***

(provide 'go-eldoc-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; go-eldoc-autoloads.el ends here
