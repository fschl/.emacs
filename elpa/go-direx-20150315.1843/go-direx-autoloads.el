;;; go-direx-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (go-direx-switch-to-buffer go-direx-pop-to-buffer)
;;;;;;  "go-direx" "go-direx.el" (21788 4529 701818 155000))
;;; Generated autoloads from go-direx.el

(autoload 'go-direx-pop-to-buffer "go-direx" "\


\(fn)" t nil)

(autoload 'go-direx-switch-to-buffer "go-direx" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("go-direx-pkg.el") (21788 4529 725914
;;;;;;  103000))

;;;***

(provide 'go-direx-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; go-direx-autoloads.el ends here
